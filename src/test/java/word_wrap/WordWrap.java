package word_wrap;

/**
 * Created by Eelco de Jong on 18-Jul-17.
 * Kata implementation.
 */

class WordWrap {

    private static final String WHITE_SPACE = " ";
    private static final String LINE_BREAK = "\n";
    private String left_sentence;
    private String right_sentence = "";
    private String result_sentence = "";

    String parse(String sentence, int line_break) {
        if (sentence.length() > line_break) {
            left_sentence = getLeftSentence(sentence, line_break);
            right_sentence = getRightSentence(sentence);
            result_sentence += left_sentence + LINE_BREAK + parse(right_sentence, line_break);
        } else {
            result_sentence += sentence;
        }
        return result_sentence;
    }

    private String getLeftSentence(String sentence, int line_break) {
        left_sentence = sentence.substring(0, line_break);
        if (left_sentence.contains(WHITE_SPACE)) {
            left_sentence = sentence.substring(0, left_sentence.lastIndexOf(WHITE_SPACE));
        }
        return left_sentence;
    }

    private String getRightSentence(String sentence) {
        right_sentence = sentence.substring(left_sentence.length(), sentence.length());
        right_sentence = right_sentence.trim();
        return right_sentence;
    }

}
