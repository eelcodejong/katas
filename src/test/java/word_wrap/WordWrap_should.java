package word_wrap;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Eelco de Jong on 18-Jul-17.
 * <p>
 * Test cases:
 * 1. Return a single word | test, 4 >> test
 * 2. Return a single word split on column | te, 1 >> t/ne
 * 3. Return a single word split multiple times | test, 1 >> t\n\e\n\s\n\t
 * 4. Return a sentence split on space | test test, 4 >> test\ntest
 * 5. Return a sentence split on space before linebreak | test test, 8 >> test\ntest
 * 6. Return a sentence split on last space | test test, 8 >> test\ntest
 * 7. Return a sentence split on last space before column | test test test, 12 >> test test\ntest
 * 8. Return a full sentence split multiple times
 */

public class WordWrap_should {

    private WordWrap wordWrap;

    @Before
    public void setUp() throws Exception {
        wordWrap = new WordWrap();
    }

    @Test
    public void return_single_word() throws Exception {
        assertThat(wordWrap.parse("test", 4), is("test"));
    }

    @Test
    public void return_single_word_split_on_line_break() throws Exception {
        assertThat(wordWrap.parse("te", 1), is("t\ne"));
    }

    @Test
    public void return_single_word_split_multiple_times_on_line_break() throws Exception {
        assertThat(wordWrap.parse("test", 1),is("t\ne\ns\nt"));
    }

    @Test
    public void return_sentence_split_on_space() throws Exception {
        assertThat(wordWrap.parse("test test", 4),is("test\ntest"));
    }

    @Test
    public void return_sentence_split_on_space_before_linebreak() throws Exception {
        assertThat(wordWrap.parse("test test", 8), is("test\ntest"));
    }

    @Test
    public void return_a_sentence_split_on_last_space() throws Exception {
        assertThat(wordWrap.parse("test test test", 12),is("test test\ntest"));
    }

    @Test
    public void return_a_full_sentence_split_multiple_times() throws Exception {
        assertThat(wordWrap.parse("Testers need to test things so they can check whether its working as it is supposed to work",
                4),is("Test\ners\nneed\nto\ntest\nthin\ngs\nso\nthey\ncan\nchec\nk\nwhet\nher\nits\nwork\ning\nas\nit\nis\nsupp\nosed\nto\nwork"));
    }
}
